import java.util.Scanner;

/**
 * [JAVA13][ДЗ№2 ч 1] - задача на проверку от Кудрина Сергея Сергеевича
 * Extra Task #1
 * <p>
 * Создать программу генерирующую пароль.
 * На вход подается число N — длина желаемого пароля. Необходимо проверить,
 * что N >= 8, иначе вывести на экран "Пароль с N количеством символов
 * небезопасен" (подставить вместо N число) и предложить пользователю еще раз
 * ввести число N.
 * Если N >= 8 то сгенерировать пароль, удовлетворяющий условиям ниже и
 * вывести его на экран. В пароле должны быть:
 * ● заглавные латинские символы
 * ● строчные латинские символы
 * ● числа
 * ● специальные знаки(_*-)
 */

public class Extra1 {
    public static final String upperChar = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    public static final String lowerChar = "abcdefghijklmnopqrstuvwxyz";
    public static final String numbers = "1234567890";
    public static final String specialChar = "!#$%&'()*+,-./:;<=>?@[\\]^_`{|}~\"";
    public static final String allChar = upperChar + lowerChar + numbers + specialChar;

    public static void main(String[] args) {
        int length = askPasswordLength();
        char[] password = new char[length];
        addSymbols(password);
        shufflePassword(password);
        printPassword(password);
    }

    //В этом методе происходит запрос необходимой длины пароля и проверка на то, достаточна ли длинна
    public static int askPasswordLength() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите желаемую длину пароля");
        while (true) {
            int length = sc.nextInt();
            if (length < 8)
                System.out.printf("Пароль с количеством символов %s небезопасен, попробуйте еще раз\n", length);
            else return length;
        }
    }

    //Данный метод служит для заполнения массива случайными из доступных символов, первые 4 заполняются фиксированными категориями
    public static void addSymbols(char[] password) {
        password[0] = upperChar.charAt((int) (Math.random() * upperChar.length()));
        password[1] = lowerChar.charAt((int) (Math.random() * lowerChar.length()));
        password[2] = numbers.charAt((int) (Math.random() * numbers.length()));
        password[3] = specialChar.charAt((int) (Math.random() * specialChar.length()));
        for (int i = 4; i < password.length; i++) {
            password[i] = allChar.charAt((int) (Math.random() * allChar.length()));
        }
    }

    //Метод перемешивает пароль
    public static void shufflePassword(char[] password){
        for (int i = 0; i < password.length; i++) {
            int randomInd = (int) (Math.random() * password.length);
            char temp = password[i];
            password[i] = password[randomInd];
            password[randomInd] = temp;
        }
    }

    public static void printPassword(char[] password) {
        System.out.print("Ваш пароль: ");
        for (char c : password) {
            System.out.print(c);
        }
    }
}
