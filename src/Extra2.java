import java.util.Scanner;

/**
 * [JAVA13][ДЗ№2 ч 1] - задача на проверку от Кудрина Сергея Сергеевича
 * Extra Task #2
 * Решить задачу 7 основного дз за линейное время
 * На вход подается число N — длина массива. Затем передается массив
 * целых чисел (ai) из N элементов, отсортированный по возрастанию.
 * Необходимо создать массив, полученный из исходного возведением в квадрат
 * каждого элемента, упорядочить элементы по возрастанию и вывести их на
 * экран.
 */

public class Extra2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] arr = new int[n];
        //Ввод массива
        for (int i = 0; i < n; i++) {
            arr[i] = sc.nextInt();
        }

        int[] arrSorted = new int[n];
        int tempInd = 0;
        int temp = arr[0];
        /*Ищем число с ближайшим к 0 модулем. Так как нам подается уже отсортированный массив, то на основе этого мы
        можем сразу заполнить первый элемент и потом двигаться от него в разные стороны по массиву*/
        for (int i = 0; i < n; i++) {
            if (Math.abs(arr[i]) < Math.abs(temp)) {
                temp = arr[i];
                tempInd = i;
            }
        }

        arrSorted[0] = temp;
        int tempInd1 = tempInd + 1;
        int tempInd2 = tempInd - 1;
        /*В случае использования вложенного цикла время работы программы будет квадратичным, поэтому
        используем один цикл и в нем через if-else сравниваем числа, находящиеся по разные стороны от
        ближайшего к 0(которое мы уже записали в 0 индекс отдельного массива). То число, которое меньше по
        модулю записывается в следующий индекс нового массива, после чего мы сдвигаемся к следующему числу по
        эту сторону от ближайшего к 0. Так мы выстраиваем новый массив, который отсортирован по модулю
        и, если я правильно понял, за линейное время.*/
        for (int i = 1; i < n; i++) {
            if (tempInd1 < n && tempInd2 > -1) {
                if (Math.abs(arr[tempInd1]) > Math.abs(arr[tempInd2])) {
                    arrSorted[i] = arr[tempInd2];
                    tempInd2--;
                } else if (Math.abs(arr[tempInd1]) <= Math.abs(arr[tempInd2])) {
                    arrSorted[i] = arr[tempInd1];
                    tempInd1++;
                }
            } else if (tempInd1 < n) {
                arrSorted[i] = arr[tempInd1];
                tempInd1++;
            } else if (tempInd2 > -1) {
                arrSorted[i] = arr[tempInd2];
                tempInd2--;
            }
        }
        //Возводим в квадрат каждый элемент массива
        for (int i = 0; i < n; i++) {
            arrSorted[i] *= arrSorted[i];
        }
        //Вывод на печать
        for (int i = 0; i < n; i++) {
            System.out.print(arrSorted[i] + " ");
        }
    }
}
